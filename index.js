"use strict";

function lightblue() {
  document.querySelector("*").style.background = "lightblue";
}
function yellow() {
  document.querySelector("*").style.background = "yellow";
}
function red() {
  document.querySelector("*").style.background = "red";
}
function blue() {
  document.querySelector("*").style.background = "blue";
}
function lightgreen() {
  document.querySelector("*").style.background = "lightgreen";
}
function blueviolet() {
  document.querySelector("*").style.background = "blueviolet";
}
